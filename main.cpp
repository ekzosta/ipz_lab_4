#include <iostream>

#include "Spisok.h"


const int mlength = 24;



#include "Spisok.cpp"

int main(){


	/////////////////ObjA/////////////////////
	Spisok* ObjA = new Spisok;

	std::cout<<"Введіть текст:"<<std::endl;
	ObjA->Insert();

	std::cout<<"Поточний текст:"<<std::endl;
	ObjA->Print();


	if(*(ObjA->selector(5))!=*(ObjA->selector(3))){
		std::cout<<ObjA->getSymbol(5)<<"!="<<ObjA->getSymbol(3)<<std::endl;
	}else{
		std::cout<<ObjA->getSymbol(5)<<"=="<<ObjA->getSymbol(3)<<std::endl;
	}

	(*ObjA)[5];

	std::cout<<std::endl<<"Поточний текст:"<<std::endl;
	ObjA->Print();



	/////////////////ObjB/////////////////////
	Spisok* ObjB = ObjA;

	std::cout<<"Поточний текст:"<<std::endl;
	ObjB->Print();

	if(*(ObjB->selector(5))!=*(ObjB->selector(3))){
		std::cout<<ObjB->getSymbol(5)<<"!="<<ObjB->getSymbol(3)<<std::endl;
	}else{
		std::cout<<ObjB->getSymbol(5)<<"=="<<ObjB->getSymbol(3)<<std::endl;
	}

	(*ObjB)[5];
	(*ObjB)('d',2);

	std::cout<<std::endl<<"Поточний текст:"<<std::endl;
	ObjB->Print();

	return 0;
}