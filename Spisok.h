class Spisok{
	char symbol;
	Spisok* next;

public:	
	Spisok();
	Spisok(char _symbol);
	~Spisok(){
		// std::cout<<"deleted "<<symbol;
	}

	void operator[](int);
	void operator()(char,int);
	bool operator!=(Spisok&) const;

	Spisok* selector(int);

	char getSymbol(int);

	int Insert();
	void Print();

};